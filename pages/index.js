import MainLayout from "../src/layouts/MainLayout";
import LoginView from "../src/views/auth/LoginView";

export default function Index() {
    return (
        <MainLayout>
            <LoginView />
        </MainLayout>
    );
}
