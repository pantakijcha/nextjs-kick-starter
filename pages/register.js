import MainLayout from "../src/layouts/MainLayout";
import RegisterView from "../src/views/auth/RegisterView";

export default function Index() {
    return (
        <MainLayout>
            <RegisterView />
        </MainLayout>
    );
}
