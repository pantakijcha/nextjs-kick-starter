import DashboardLayout from "../../../src/layouts/DashboardLayout";
import SettingsView from "../../../src/views/settings/SettingsView";

export default function Dashboard() {
    return (
        <DashboardLayout>
            <SettingsView />
        </DashboardLayout>
    );
}
