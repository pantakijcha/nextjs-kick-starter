import DashboardLayout from "../../../src/layouts/DashboardLayout";
import DashboardView from "../../../src/views/reports/DashboardView";

export default function Dashboard() {
    return (
        <DashboardLayout>
            <DashboardView />
        </DashboardLayout>
    );
}
