import DashboardLayout from "../../../src/layouts/DashboardLayout";
import ProductListView from "../../../src/views/product/ProductListView";

export default function Dashboard() {
    return (
        <DashboardLayout>
            <ProductListView />
        </DashboardLayout>
    );
}
