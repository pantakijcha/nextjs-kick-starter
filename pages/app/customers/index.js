import DashboardLayout from "../../../src/layouts/DashboardLayout";
import CustomerListView from "../../../src/views/customer/CustomerListView";

export default function Dashboard() {
    return (
        <DashboardLayout>
            <CustomerListView />
        </DashboardLayout>
    );
}
