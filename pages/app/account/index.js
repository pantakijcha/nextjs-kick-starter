import DashboardLayout from "../../../src/layouts/DashboardLayout";
import AccountView from "../../../src/views/account/AccountView";

export default function Dashboard() {
    return (
        <DashboardLayout>
            <AccountView />
        </DashboardLayout>
    );
}
