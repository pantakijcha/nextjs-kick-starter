import NotFoundView from "../src/views/errors/NotFoundView";

export default function Index() {
    return <NotFoundView />;
}
